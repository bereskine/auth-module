import app.auth.messages.CustomerApiToken
import app.auth.messages.CustomerError
import app.auth.messages.LoginCustomer
import app.auth.model.ApiToken
import app.auth.model.ErrorDetails
import app.auth.model.UserCredentials
import com.fasterxml.jackson.databind.ObjectMapper

ObjectMapper mapper = new ObjectMapper()

println(mapper.writeValueAsString(new UserCredentials("john", "123")))
println()

def errorDetails = new ErrorDetails("customer.notFound", "Customer not found")
println(mapper.writeValueAsString(errorDetails))
println()

def apiToken = new ApiToken(UUID.randomUUID().toString(), System.currentTimeMillis())
println(mapper.writeValueAsString(apiToken))
println()

def login = new LoginCustomer("john", "123")
println(mapper.writeValueAsString(login))
println()

def customerApiToken = new CustomerApiToken(login, apiToken)
println(mapper.writeValueAsString(customerApiToken))
println()

def customerError = new CustomerError(login, errorDetails)
println(mapper.writeValueAsString(customerError))
println()