import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value

import java.util.concurrent.TimeUnit

/**
 *
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 * Created: 07.10.2015 00:30
 */
class AuthServiceTest extends BaseTest {

    @Value("\${apiTokenLifeTimeUnit}")
    TimeUnit apiTokenLifeTimeUnit;

    @Value("\${apiTokenLifeTime}")
    long apiTokenLifeTime;

    @Value("\${invalidateExpiredSessionsDelay}")
    long invalidateExpiredSessionsDelay;

    @Before
    void init() {
        userRepository.deleteAll()
        userService.newUser(email, passwd)
    }

    @Test
    void loginSuccessTest() {
        def apiToken = authService.requestApiToken(email, passwd).get()
        log.info(apiToken.toString())
        Assert.assertTrue(authService.isTokenValid(apiToken.value))
    }

    @Test
    void loginTwiceSuccessTest() {
        def apiToken_1 = authService.requestApiToken(email, passwd).get()
        log.info(apiToken_1.toString())
        def apiToken_2 = authService.requestApiToken(email, passwd).get()
        log.info(apiToken_2.toString())

        Assert.assertNotEquals(apiToken_1.value, apiToken_2.value)
        Assert.assertTrue(authService.isTokenValid(apiToken_2.value))
        Assert.assertFalse(authService.isTokenValid(apiToken_1.value))
    }

    @Test
    void expireTokenTest() {
        def apiToken = authService.requestApiToken(email, passwd).get()
        Thread.sleep(apiTokenLifeTimeUnit.toMillis(apiTokenLifeTime) + invalidateExpiredSessionsDelay * 2)
        Assert.assertFalse(authService.isTokenValid(apiToken.value))
    }

    @Test
    void test(){
        apiTokenLogRepository.findByExpirationDateGreaterThanOrderByExpirationDateDesc(new Date())
                .forEach({it -> println(it)});
    }

    @Test
    void postConstructTest() {
        def email_2 = "2_" + email
        userService.newUser(email_2, passwd)

        def apiToken = authService.requestApiToken(email, passwd).get()
        def apiToken_2 = authService.requestApiToken(email_2, passwd).get()
        def apiToken_3 = authService.requestApiToken(email_2, passwd).get()

        Thread.sleep(1000)
        authService.populateSessionsFromDB()

        Assert.assertTrue(authService.isTokenValid(apiToken.value))
        Assert.assertFalse(authService.isTokenValid(apiToken_2.value))
        Assert.assertTrue(authService.isTokenValid(apiToken_3.value))
    }
}
