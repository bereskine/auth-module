import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.springframework.dao.DataIntegrityViolationException

/**
 *
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 * Created: 07.10.2015 00:01
 */
class UserServiceTest extends BaseTest {

    @Before
    void init() {
        userRepository.deleteAll()
    }

    @Test
    void newUserSuccsessTest() {
        def user = userService.newUser(email, passwd)
        def loadedUser = userService.findUser(user.email)
        Assert.assertEquals(user.email, loadedUser.email)
        Assert.assertEquals(userService.passwdHash(passwd), loadedUser.passwdHash)
    }

    @Test(expected = DataIntegrityViolationException.class)
    void newUserFailureTest() {
        userService.newUser(email, passwd)
        userService.newUser(email, passwd)
    }

    @Test
    void findNewUserTest() {
        Assert.assertEquals(userService.newUser(email, passwd), userService.findUser(email))
    }

    @Test
    void updatePasswdTest() {
        userService.newUser(email, passwd)
        def newPasswd = "newPasswd"
        userService.updatePassword(email, newPasswd)
        Assert.assertEquals(userService.passwdHash(newPasswd), userService.findUser(email).passwdHash)
    }

}
