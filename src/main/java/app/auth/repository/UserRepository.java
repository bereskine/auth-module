package app.auth.repository;

import app.auth.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 20.08.12 17:28
 */
public interface UserRepository extends CrudRepository<User, Integer> {

    User findByEmail(String email);

}
