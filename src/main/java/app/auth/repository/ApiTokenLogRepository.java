package app.auth.repository;

import app.auth.entity.ApiTokenLog;
import app.auth.entity.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 20.08.12 17:28
 */
public interface ApiTokenLogRepository extends CrudRepository<ApiTokenLog, Integer> {

    @EntityGraph(value = "ApiTokenLog.detail", type = EntityGraph.EntityGraphType.LOAD)
    List<ApiTokenLog> findByExpirationDateGreaterThanOrderByExpirationDateDesc(Date date);

}
