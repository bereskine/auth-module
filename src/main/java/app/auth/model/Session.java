package app.auth.model;

import app.auth.entity.User;

import java.util.*;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 06.10.2015 23:23
 */
public class Session {

    public final User user;
    public final ApiToken apiToken;

    public Session(User user, ApiToken apiToken) {
        this.user = user;
        this.apiToken = apiToken;
    }

    @Override
    public String toString() {
        return "Session{" +
                "user=" + user +
                ", apiToken=" + apiToken +
                '}';
    }
}
