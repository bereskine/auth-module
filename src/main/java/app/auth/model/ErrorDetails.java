package app.auth.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 07.10.2015 12:11
 */
public class ErrorDetails {

    @JsonProperty("error_code")
    public final String code;

    @JsonProperty("error_description")
    public final String description;

    public ErrorDetails(String code, String description) {
        this.code = code;
        this.description = description;
    }

    @Override
    public String toString() {
        return "ErrorDetails{" +
                "code='" + code + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
