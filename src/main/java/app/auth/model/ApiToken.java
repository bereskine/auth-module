package app.auth.model;

import app.auth.common.AppUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 06.10.2015 22:56
 */
public class ApiToken {

    @JsonProperty("api_token")
    public final String value;

    @JsonIgnore
    public final long expirationDate;

    @JsonProperty("api_token_expiration_date")
    public String formatExpirationDate(){
       return AppUtils.formatJsonDate(expirationDate);
    }

    public ApiToken(String value, long expirationDate) {
        this.value = value;
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return "ApiToken{" +
                "value='" + value + '\'' +
                ", expirationDate=" + AppUtils.formatDate(expirationDate) +
                '}';
    }
}
