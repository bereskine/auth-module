package app.auth.controller;

import app.auth.common.AppError;
import app.auth.messages.CustomerApiToken;
import app.auth.messages.CustomerError;
import app.auth.messages.LoginCustomer;
import app.auth.messages.Message;
import app.auth.service.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 07.10.2015 12:16
 */
@Controller
public class AuthController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    AuthService authService;

    @MessageMapping("/login")
    @SendTo("/topic/app")
    public Message loginCustomer(LoginCustomer login) throws Exception {
        log.info("<< {}", login);
        Message response;
        try {
            response = authService.requestApiToken(login.data)
                    .map(apiToken -> (Message) new CustomerApiToken(login, apiToken))
                    .orElse(new CustomerError(login, AppError.CustomerNotFound.errorDetails));
        } catch (Exception e) {
            log.error(e.toString(), e);
            response = new CustomerError(login, AppError.AppError.errorDetails);
        }
        log.info(">> {}", response);
        return response;
    }

}
