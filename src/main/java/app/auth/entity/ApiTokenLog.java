package app.auth.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 06.10.2015 21:29
 */
@Entity
@NamedEntityGraph(name = "ApiTokenLog.detail",  attributeNodes = @NamedAttributeNode("user"))
public class ApiTokenLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date logDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", updatable = false)
    private User user;

    @Column(nullable = false, updatable = false)
    private String apiToken;

    @Column(nullable = false, updatable = false)
    private Date expirationDate;

    public ApiTokenLog() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getLogDate() {
        return logDate;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return "ApiTokenLog{" +
                "id=" + id +
                ", logDate=" + logDate +
                ", user=" + user +
                ", apiToken='" + apiToken + '\'' +
                ", expirationDate=" + expirationDate +
                '}';
    }
}
