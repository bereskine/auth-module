package app.auth.service;

import app.auth.entity.ApiTokenLog;
import app.auth.entity.User;
import app.auth.model.ApiToken;
import app.auth.model.Session;
import app.auth.model.UserCredentials;
import app.auth.repository.ApiTokenLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 06.10.2015 22:54
 */
@Service
public class AuthService {

    @Autowired
    UserService userService;

    @Autowired
    ApiTokenLogRepository apiTokenLogRepository;

    @Value("${apiTokenLifeTimeUnit:MINUTES}")
    TimeUnit apiTokenLifeTimeUnit;

    @Value("${apiTokenLifeTime:30}")
    long apiTokenLifeTime;

    private Map<String, Session> sessionsByToken = new ConcurrentHashMap<>();
    private Map<String, Session> sessionsByEmail = new ConcurrentHashMap<>();

    public Optional<ApiToken> requestApiToken(UserCredentials userCredentials) {
        return userService.authUser(userCredentials.email, userCredentials.password).map(user -> {
            //инвалидируем текущую сессию если она есть
            Optional.ofNullable(sessionsByEmail.remove(userCredentials.email)).ifPresent(session -> sessionsByToken.remove(session.apiToken.value));

            ApiToken apiToken = new ApiToken(UUID.randomUUID().toString(), System.currentTimeMillis() + apiTokenLifeTimeUnit.toMillis(apiTokenLifeTime));
            Session session = new Session(user, apiToken);
            sessionsByToken.put(apiToken.value, session);
            sessionsByEmail.put(user.getEmail(), session);

            logNewApiToken(apiToken, user);

            return apiToken;
        });
    }

    @PostConstruct
    public void populateSessionsFromDB() {
        Map<String, Session> sessionsByToken = new ConcurrentHashMap<>();
        Map<String, Session> sessionsByEmail = new ConcurrentHashMap<>();
        apiTokenLogRepository.findByExpirationDateGreaterThanOrderByExpirationDateDesc(new Date()).stream()
                .filter(apiTokenLog -> sessionsByEmail.get(apiTokenLog.getUser().getEmail()) == null)
                .forEach(apiTokenLog -> {
                    ApiToken apiToken = new ApiToken(apiTokenLog.getApiToken(), apiTokenLog.getExpirationDate().getTime());
                    Session session = new Session(apiTokenLog.getUser(), apiToken);
                    sessionsByToken.put(apiToken.value, session);
                    sessionsByEmail.put(apiTokenLog.getUser().getEmail(), session);
                });
        this.sessionsByToken = sessionsByToken;
        this.sessionsByEmail = sessionsByEmail;
    }

    public boolean isTokenValid(String token) {
        Session session = sessionsByToken.get(token);
        return session != null && session.apiToken.expirationDate >= System.currentTimeMillis();
    }

    @Transactional
    @Async
    private void logNewApiToken(ApiToken apiToken, User user) {
        ApiTokenLog apiTokenLog = new ApiTokenLog();
        apiTokenLog.setUser(user);
        apiTokenLog.setLogDate(new Date());
        apiTokenLog.setApiToken(apiToken.value);
        apiTokenLog.setExpirationDate(new Date(apiToken.expirationDate));

        apiTokenLogRepository.save(apiTokenLog);
    }

    @Scheduled(fixedDelayString = "${invalidateExpiredSessionsDelay:60000}")
    private void invalidateExpiredSessions() {
        for(Session session : sessionsByToken.values()) {
            if(System.currentTimeMillis() > session.apiToken.expirationDate) {
                sessionsByToken.remove(session.apiToken.value);
                sessionsByEmail.remove(session.user.getEmail());
            }
        }
    }

}
