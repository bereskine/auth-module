package app.auth.service;

import app.auth.entity.User;
import app.auth.repository.UserRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 06.10.2015 22:12
 */
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Value("${userPasswdSalt}")
    String salt;

    public Optional<User> authUser(String email, String passwd) {
       return Optional.ofNullable(findUser(email)).filter(user -> user.getPasswdHash().equals(passwdHash(passwd)));
    }

    @Cacheable("users")
    public User findUser(String email) {
        return userRepository.findByEmail(email);
    }

    @CachePut(value = "users", key = "#email")
    @Transactional
    public User updatePassword(String email, String password) {
        User user = userRepository.findByEmail(email);
        if(user != null) {
            user.setPasswdHash(passwdHash(password));
            userRepository.save(user);
        }
        return user;
    }

    @CachePut(value = "users", key = "#email")
    @Transactional
    public User newUser(String email, String password) {
        return userRepository.save(new User(email, passwdHash(password)));
    }

    public String passwdHash(String password){
        return DigestUtils.sha1Hex(password + salt);
    }

}
