package app.auth.messages;

import app.auth.model.ApiToken;

import java.util.*;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 07.10.2015 12:10
 */
public class CustomerApiToken extends Message<ApiToken> {

    public CustomerApiToken(Message request, ApiToken data) {
        this.type = Type.CUSTOMER_API_TOKEN.name();
        this.sequenceId = request.sequenceId;
        this.data = data;
    }

}
