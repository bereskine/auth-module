package app.auth.messages;

import app.auth.model.ApiToken;
import app.auth.model.ErrorDetails;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 07.10.2015 12:10
 */
public class CustomerError extends Message<ErrorDetails> {

    public CustomerError(Message request, ErrorDetails data) {
        this.type = Type.CUSTOMER_ERROR.name();
        this.sequenceId = request.sequenceId;
        this.data = data;
    }

}
