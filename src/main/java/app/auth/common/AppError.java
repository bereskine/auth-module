package app.auth.common;

import app.auth.model.ErrorDetails;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 07.10.2015 14:01
 */
public enum AppError {

    CustomerNotFound(new ErrorDetails("customer.notFound", "Customer not found")),
    AppError(new ErrorDetails("app.error", "Internal server error")),;

    public final ErrorDetails errorDetails;

    AppError(ErrorDetails errorDetails) {
        this.errorDetails = errorDetails;
    }
}
