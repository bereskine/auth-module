CREATE TABLE "user"
(
  id serial NOT NULL,
  email character varying(255) NOT NULL,
  passwd_hash character varying(40) NOT NULL,
  CONSTRAINT user_pkey PRIMARY KEY (id),
  CONSTRAINT user_email_key UNIQUE (email)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "user"
  OWNER TO postgres;

CREATE TABLE api_token_log
(
  id serial NOT NULL,
  log_date timestamp without time zone NOT NULL DEFAULT now(),
  user_id integer NOT NULL,
  api_token character varying(36) NOT NULL,
  expiration_date timestamp without time zone NOT NULL,
  CONSTRAINT api_token_log_pkey PRIMARY KEY (id),
  CONSTRAINT api_token_log_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES "user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE api_token_log
  OWNER TO postgres;

insert into "user" (email, passwd_hash) VALUES ('fpi@bk.ru', '4febfac1c5a06b2387b7db8b8d56d302fe3300ca');